import React from 'react';

const List = ({process}) => {
  if (process.length < 1) return null;

  return (
    <table className="highlight centered">
      <thead>
        <tr>
          <th>PID</th>
          <th>TS</th>
        </tr>
      </thead>
      <tbody>
        {process.map((proc) => (
          <tr key={proc.pid}>
            <td>{proc.pid}</td>
            <td>{proc.tsAux}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default List;
