import React from 'react';

const Table2 = ({process}) => {
  let index = 1;
  var rows = process.map((proc) => {
    return (
      <tr key={(index - 1).toString() + proc.pid.toString()}>
        <td>{proc['pid']}</td>
        <td>{proc['te']}</td>
        <td>{proc['tr']}</td>
      </tr>
    );
  });

  return (
    <table className="highlight centered">
      <thead>
        <tr>
          <th>PID</th>
          <th>Tiempo de espera</th>
          <th>Tiempo de retorno</th>
        </tr>
      </thead>
      <tbody>{rows}</tbody>
    </table>
  );
};

export default Table2;
