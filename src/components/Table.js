import React from 'react';

const Table = ({process, result, show}) => {
  let rows = [];

  let hasChanged = true;

  let index = 1;
  if (result && show) {
    while (hasChanged) {
      hasChanged = false;

      for (let i = 0; i < result.length; i++) {
        const proc = result[i];

        if (proc['stages'][index - 1]) {
          hasChanged = true;
          let row = (
            <tr key={(index - 1).toString() + proc.pid.toString()}>
              <td>{proc.pid}</td>
              <td>{proc['stages'][index - 1]['ts']}</td>
              <td>{index}</td>
            </tr>
          );
          rows.push(row);
        }
      }

      index++;
    }
  }

  return (
    <table className="highlight centered">
      <thead>
        <tr>
          <th>PID</th>
          <th>Tiempo de servicio (Restante)</th>
          <th>Vuelta</th>
        </tr>
      </thead>
      <tbody>{rows}</tbody>
    </table>
  );
};

export default Table;
