import React, {useState} from 'react';
import Button from '@material-ui/core/Button';

const Form = ({addProcess}) => {
  //state process
  const [process, setProcess] = useState({});
  const {pid, ts} = process;

  //state error
  const [error, setError] = useState(false);

  const eventType = (e) => {
    setProcess({
      ...process,
      [e.target.name]: parseInt(e.target.value),
    });
  };

  const eventSubmit = (e) => {
    e.preventDefault();

    if (!pid || !ts || pid < 1 || ts < 1) {
      setError(true);
    } else {
      setError(false);

      addProcess({
        ...process,
        te: 0,
        tr: 0,
        stages: [],
        tsAux: process['ts'],
      });
    }
  };

  return (
    <form onSubmit={eventSubmit}>
      <div className="input-field col s12">
        <div className="input-field col s12">
          <input
            name="pid"
            id="pid"
            type="number"
            className="validate"
            onChange={eventType}
          />
          <label htmlFor="pid">ProcessID (PID)</label>
        </div>

        <div className="input-field col s12">
          <input
            name="ts"
            id="ts"
            type="number"
            className="validate"
            onChange={eventType}
          />
          <label htmlFor="ts">Service Time (TS)</label>
        </div>

        <Button
          type="submit"
          variant="contained"
          color="primary"
          className="col m12"
        >
          Add Process
        </Button>
      </div>
    </form>
  );
};

export default Form;
