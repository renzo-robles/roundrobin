/*
ti = tiempo de intercambio, en este caso esta dividido en 2 partes :
     tiempo para sacar un proceso y tiempo para poner uno nuevo  
*/
export function roundRobin(processList, quantum, ti) {
  var time = 0;

  var listaTerminados = [];

  var pidAnt = -1;

  while (processList.length > 0) {
    for (let index = 0; index < processList.length; index++) {
      var process = processList[index];

      time += ti / 2; //tiempo que toma realizar el cambio de contexto para agregar el nuevo proceso

      if (pidAnt != process['pid']) {
        process['te'] = process['te'] + time;
      }

      pidAnt = process['pid'];

      if (process.ts > quantum) {
        process.ts = process.ts - quantum;

        time += quantum; //tiempo de procesamiento
      } else {
        time += process.ts;
        process.ts = 0;

        for (let i = 0; i < processList.length; i++) {
          let proc = processList[i];

          if (proc.pid == process.pid) {
            let procesoTerminado = processList.splice(i, 1)[0];
            procesoTerminado['tr'] =
              procesoTerminado['te'] + procesoTerminado['tsAux'];

            listaTerminados.push(procesoTerminado);
          }
        }
      }

      time += ti / 2; //tiempo en cambiar de contexto, sacando el proceso actual

      process['stages'].push({...process, stages: []});
    }
  }

  return listaTerminados;
}

function eliminarProceso() {}
