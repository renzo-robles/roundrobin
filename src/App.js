import React, {useState} from 'react';
import './App.css';
import Button from '@material-ui/core/Button';
import Form from './components/Form';
import List from './components/List';
import Table from './components/Table';
import Table2 from './components/Table2';

import {roundRobin, merge} from './helpers/roundRobin';

function App() {
  const [process, setProcess] = useState([]);
  const [result, setResult] = useState([]);

  const [show, setShow] = useState(false);

  const [mostrarTabla2, setMostrarTabla2] = useState(false);

  const addProcess = (proc) => {
    setProcess([...process, proc]);
    setShow(false);
  };

  const startRoundRobin = () => {
    let res = roundRobin([...process], 10, 1);
    //hacer un merge entre res y process, para mantener el orden de la lista y mostrar en orden
    let resultadoFinal = setShow(true);

    console.log(process);
    setResult(process);

    setMostrarTabla2(true);
  };

  return (
    <div className="row">
      <div className="col m2">
        <div className="col m12">
          <Form addProcess={addProcess} />
        </div>
        <div className="col m12">
          <Button
            onClick={startRoundRobin}
            variant="contained"
            color="secondary"
            className="col m12"
          >
            Start
          </Button>
        </div>
        <div className="col m12">
          <List process={process} />
        </div>
      </div>

      <div className="col m10">
        <Table result={result} process={process} show={show} />
      </div>

      {mostrarTabla2 ? (
        <div className="col m10 tabla2">
          <Table2 process={process} />
        </div>
      ) : null}
    </div>
  );
}

export default App;
